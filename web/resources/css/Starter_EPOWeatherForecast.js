function readWeatherForecast(aid) {

    var webMethod = "/" + utility + "/script/service.asmx/readStarterWeatherData";
    var parameters = "{'context':'" + context + "','ssn':'" + "" + "','userid':'" + entityid + "','aid':'" + aid + "','uom':'" + "F" + "',dailyforecast:'1'}";

    jQuery.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8;",
        url: webMethod,
        data: parameters,
        dataType: "json",
        async: true,
        cache: false,
        success: function (msg) {

            if (msg.d == "") {
                document.getElementById('divcurrentweather').innerHTML = "Please verify if account has address setup.";
                jQuery('#weather #divBusy-placeholder').hide();
            } else
                processWeatherForecastData(msg.d);
            
        },
        error: function (e) {
        }
    });
}

function processWeatherForecastData(objJson) {

    var weatherForecasts; var currentForecastHtml = "";
    var retJsonData = JSON.parse(objJson);
    var current_placename; var current_humidity;
    var current_temp; var current_tempuom; var current_feelslike;
    var current_weatherdesc; var current_weathercode;
    var current_windspeed; var current_winduom;

    jQuery.each(retJsonData, function (key, val) {
        //adWidget = val.widget;
        weatherForecasts = val.forecastdays;
        current_placename = val.placename; 
        current_humidity = val.humidity;
        current_temp = val.currenttemp;
        current_tempuom = val.tempuom;
        current_feelslike = val.feelsliketemp;
        current_weatherdesc = val.weatherdesc; 
        current_weathercode = val.weathercode;
        current_windspeed = val.windspeed;
        current_winduom = val.winduom;        
    });
  
    currentForecastHtml += '<h5>' + current_placename + '</h5>';
    currentForecastHtml += '<div class="col-xs-8 col-sm-7">';
    currentForecastHtml += '<div class="row">';
    currentForecastHtml += '<div class="col-xs-6 col-sm-6">';
    if (parseInt(current_weathercode) == 10 || parseInt(current_weathercode) == 23)
        currentForecastHtml += '<h2 class="' + getWeatherClass(parseInt(current_weathercode)) + '"><span></span></h2>';
    else
        currentForecastHtml += '<h2 class="' + getWeatherClass(parseInt(current_weathercode)) + '"></h2>';
    currentForecastHtml += '</div>';
    currentForecastHtml += '<div class="col-xs-6 col-sm-6">';
    currentForecastHtml += '<h4>' + current_temp + '<sup>&deg;' + current_tempuom + '</sup></h4>';
    currentForecastHtml += '</div>';
    currentForecastHtml += '</div>';
    currentForecastHtml += '</div>'
    
    currentForecastHtml += '<div class="col-xs-4 col-sm-5 meta"><div class="row">';
    currentForecastHtml += '<p><strong>' + kendo.format('{0:MM/dd/yy}', new Date()) + '</strong></p>';
    //currentForecastHtml += '<p>' + kendo.format('{0:hh:mm tt}', new Date()) + '</p>';
    currentForecastHtml += '<p><strong>Feels like:</strong> <span>' + current_feelslike + '&deg;</span></p>';
    currentForecastHtml += '<p><strong>Humidity:</strong> <span>' + current_humidity + '%</span></p>';
    currentForecastHtml += '<p><strong>Wind:</strong> <span>' + current_windspeed + ' ' + current_winduom + '</span></p>';
    currentForecastHtml += '</div></div></div>';

    if (weatherForecasts) {
        var weatherTabHtml = "";

        jQuery.each(weatherForecasts, function (key, val) {
            var weatherForecastItem = val;
            jQuery.each(weatherForecastItem, function (weatherkey, weatherval) {

                var forecast_date = weatherval.forecast_date;
                var forecast_weathercode = weatherval.forecast_weathercode;
                var forecast_weatherdesc = weatherval.forecast_weatherdesc;
                var forecast_maxtemp = weatherval.forecast_maxtemp;
                var forecast_mintemp = weatherval.forecast_mintemp

                weatherTabHtml += '<li>';
                weatherTabHtml += '<p class="day">' + formatDate_DayName(forecast_date) + '</p>';

                if (parseInt(forecast_weathercode) == 10 || parseInt(forecast_weathercode) == 23)
                    weatherTabHtml += '<h2 class="' + getWeatherClass(parseInt(forecast_weathercode)) + '"><span></span></h2>';
                else
                    weatherTabHtml += '<h2 class="' + getWeatherClass(parseInt(forecast_weathercode)) + '"></h2>';

                weatherTabHtml += '<p class="temp"><span class="hi">' + forecast_maxtemp + '&deg;</span>/<span class="low">' + forecast_mintemp + '&deg;</span></p>';
                weatherTabHtml += '</li>';
            });
        });
        document.getElementById('ulForecastDays').innerHTML = weatherTabHtml;
    }

    document.getElementById('divcurrentweather').innerHTML = currentForecastHtml;
    jQuery('#weather #divBusy-placeholder').hide();

    jQuery(document).ready(loadUIElements());

}

function getWeatherClass(weathercode) {
    
    switch (parseInt(weathercode)) {
        case 0:
            return ""; //'missing'
            break;
        case 1:
            return "se-sunny";
            break;
        case 2:
            return "se-partly-cloudy";
            break;
        case 3:
            return "se-cloudy";
            break;
        case 4:
            return "se-dust";
            break;
        case 5:
            return "se-mostly-sunny";
            break;
        case 6:
            return "se-fog";
            break;
        case 7:
            return "se-hot-humid";
            break;
        case 8:
            return "se-haze";
            break;
        case 9:
            return "se-very-cold";
            break;
        case 10:
            return "se-snow-showers";
            break;
        case 11:
            return "se-smoke";
            break;
        case 12:
            return "se-drizzle";
            break;
        case 13:
            return "se-snow-flurries";
            break;
        case 14:
            return "se-windy";
            break;
        case 15:
            return "se-rain-snow-mix";
            break;
        case 16:
            return "se-blizzard";
            break;
        case 17:
            return "se-snow-flurries";
            break;
        case 18:
            return "se-rainy";
            break;
        case 19:
            return "se-snow";
            break;
        case 20:
            return "se-thunderstorms";
            break;
        case 21:
            return "se-sunny";
            break;
        case 22:
            return "se-sunny";
            break;
        case 23:
            return "se-heavy-rain-showers";
            break;
        case 24:
            return "se-sleet-snow-mix";
            break;
        case 25:
            return "se-freezing-rain";
            break;
        case 26:
            return "se-sleet-or-freezing-drizzle";
            break;
    }
}

function formatDate_DayName(dStart) {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var fDate = new Date(dStart);
    var dayname = days[fDate.getDay()];
    return dayname;
}