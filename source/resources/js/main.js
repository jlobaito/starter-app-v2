// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    jQuery('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

jQuery(document).ready(UTIL.loadEvents);






function contentResize(){
  var height = jQuery(window).height(),
  windowSize = jQuery(window).width(),
  content = jQuery('.main-content'),
  nav = jQuery('header').outerHeight(),
  contentHeight = height - nav;
  if (windowSize < 1100 ) {
    content.css('height','auto');

  } else {
    content.css('height', contentHeight);

  }

}

function forecastPosition(){
  var forecastHeight = $('#weather .forecast').outerHeight();

  $('#weather #weatherforecast .content').css('padding-bottom', forecastHeight);
}

function flexResize(){
  var height = jQuery("#energyTips").outerHeight(),
  content = jQuery('.flexslider li .row'),
  header = jQuery('h2').outerHeight(),
  contentHeight = height - header;
  content.css('height', contentHeight);

}

$(function () {
  $('[data-toggle="tooltip"]').tooltip('show')
})



function loadUIElements(){
  jQuery(".content h3").fitText(.72, {minFontSize:'22px', maxFontSize: '30px' });
  jQuery("#cost .content h3").fitText(.72, {minFontSize:'22px', maxFontSize: '25px' });
  jQuery("#ads .content h3").fitText(1, {maxFontSize: 'px' });
  jQuery(".meta, #energyUsage .content p").fitText(1.3, { minFontSize: '11px', maxFontSize: '14px' });
  jQuery(".moduleHeading").fitText(1.6, {minFontSize: '11px', maxFontSize: '18px' });
  jQuery("#energyUsage h4").fitText(.2);
  jQuery('#statistics h5').fitText(1.3, {minFontSize:'14px', maxFontSize:'24px'});
  jQuery('.content p').fitText(1.3, {minFontSize:'11px', maxFontSize:'14px'});
  jQuery('#weather .forecast .day, #weather .forecast .temp').fitText(1.3, {minFontSize:'8px', maxFontSize:'11px'});
  jQuery('[data-toggle="tooltip"]').tooltip();
  contentResize();
  flexResize();
  forecastPosition();
};

jQuery(window).resize(function(){
  contentResize();
  flexResize();
})



